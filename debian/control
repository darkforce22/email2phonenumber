Source: email2phonenumber
Section: misc
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13), dh-python, python3-setuptools, python3-all
Standards-Version: 4.6.2
Homepage: https://github.com/martinvigo/email2phonenumber
Vcs-Browser: https://gitlab.com/kalilinux/packages/email2phonenumber
Vcs-Git: https://gitlab.com/kalilinux/packages/email2phonenumber.git
Rules-Requires-Root: no

Package: email2phonenumber
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}, python3-bs4, python3-certifi, python3-chardet, python3-idna, python3-requests, python3-soupsieve, python3-urllib3
Description: OSINT tool to obtain a target's phone number by having their email address
 This package contains an OSINT tool that allows you to obtain a target's phone
 number just by having their email address.
 .
 This tool helps automate discovering someone's phone number by abusing
 password reset design weaknesses and publicly available data. It supports 3
 main functions:
 .
    * "scrape" - scrapes websites for phone number digits by initiating
      password reset using the target's email address.
    * "generate" - creates a list of valid phone numbers based on the country's
      Phone Numbering Plan publicly available information.
    * "bruteforce" - iterates over a list of phone numbers and initiates
      password reset on different websites to obtain associated masked emails
      and correlate it to the victim's one.
